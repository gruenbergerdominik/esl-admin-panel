class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
	require "google_drive"

	session = GoogleDrive::Session.from_config("config.json")

	before_action do
		Time.zone = ActiveSupport::TimeZone['Europe/Vienna']
	end

	before_action :allow_iframe_requests

	def allow_iframe_requests
		response.headers.delete('X-Frame-Options')
	end

	def checkSession
		if cookies[:_PUBG_ESLM_COOKIE_9374843].nil?
			redirect_to sessions_path
		else
			if cookies[:_PUBG_ESLM_COOKIE_9374843] == "3.14159265359"
				
			else
				redirect_to sessions_path
			end
		end
	end

	def google_sheets

		session.files.each do |file|
			puts file.title
		end
	end
end
