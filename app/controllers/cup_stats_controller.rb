class CupStatsController < ApplicationController
  before_action :set_cup_stat, only: [:show, :edit, :update, :destroy]

  # GET /cup_stats
  # GET /cup_stats.json
  def index
    @list = false
    @title = "Choose"
    case params[:list]
    when 'all'
      @cup_stats = CupStat.all.order("league ASC")
      @list = true
      @title = "All"
    when "csgo"
      @cup_stats = CupStat.where(game: "counterstrike").order("league ASC")
      @list = true
      @title = "Counter Strike"
    when "hs"
      @cup_stats = CupStat.where(game: "hearthstone").order("league ASC")
      @list = true
      @title = "Hearthstone"
    when 'rs'
      @cup_stats = CupStat.where(game: "rainbowsix").order("league ASC")
      @list = true
      @title = "Rainbow Six"
    when 'lol'
      @cup_stats = CupStat.where(game: "leagueoflegends").order("league ASC")
      @list = true
      @title = "League of Legends"
    else
      @cup_stats = CupStat.all.order("league ASC")
      @list = false
    end

  end

  # GET /cup_stats/1
  # GET /cup_stats/1.json
  def show
  end

  # GET /cup_stats/new
  def new
    @cup_stat = CupStat.new
  end

  # GET /cup_stats/1/edit
  def edit
  end

  # POST /cup_stats
  # POST /cup_stats.json
  def create
    @cup_stat = CupStat.new(cup_stat_params)

    respond_to do |format|
      if @cup_stat.save
        format.html { redirect_to @cup_stat, notice: 'Cup stat was successfully created.' }
        format.json { render :show, status: :created, location: @cup_stat }
      else
        format.html { render :new }
        format.json { render json: @cup_stat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cup_stats/1
  # PATCH/PUT /cup_stats/1.json
  def update
    respond_to do |format|
      if @cup_stat.update(cup_stat_params)
        format.html { redirect_to @cup_stat, notice: 'Cup stat was successfully updated.' }
        format.json { render :show, status: :ok, location: @cup_stat }
      else
        format.html { render :edit }
        format.json { render json: @cup_stat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cup_stats/1
  # DELETE /cup_stats/1.json
  def destroy
    @cup_stat.destroy
    respond_to do |format|
      format.html { redirect_to cup_stats_url, notice: 'Cup stat was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cup_stat
      @cup_stat = CupStat.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cup_stat_params
      params.require(:cup_stat).permit(:league, :name, :uri, :size, :mode, :timeline, :signed_up, :checked_in)
    end
end
