class RequestsController < ApplicationController
  before_action :set_request, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token, only: [:create]
  # GET /requests
  # GET /requests.json
  def index
    @requests = Request.all
  end

  # GET /requests/1
  # GET /requests/1.json
  def show
  end

  # GET /requests/new
  def new
    @request = Request.new
    @type = "user"
    if params[:type] == "team"
      @type = "team"
    end
  end

  # GET /requests/1/edit
  def edit
  end

  def get_file
  
  end

  # POST /requests
  # POST /requests.json
  def create
    temp_ids = params[:id]
    type = params[:type]
    id = nil
    more = nil
    if temp_ids.include? ","
      temp_id_array = temp_ids.split ","
      if temp_id_array[1] == ""
        id = temp_id_array[0]
        more = false
      else
        more = true
        id = temp_id_array
      end
    else
      id = temp_ids
      more = false
    end
    if more
      id.each do |id|
        tem_id = Integer(id) rescue false
        if tem_id == false
          render json: {status: 400, result: "One of the ID's is not a valid ID"}
          return
        end
      end
    end
    #filds
    link = "https://api.eslgaming.com/play/v1/users?ids=<ids>&fields=<fields>" if type == "user"
    link = "https://api.eslgaming.com/play/v1/teams?ids=<ids>&fields=<fields>" if type == "team"
    #id,alias,nickname,region,photo,premium,trustLevel,level,awards
    field = "id"
    puts params[:alias]
    field = field + ",alias" if params[:alias] == "true"
    field = field + ",nickname" if params[:nickname] == "true"
    field = field + ",region" if params[:region] == "true"
    field = field + ",photo" if params[:photo] == "true"
    field = field + ",premium" if params[:premium] == "true"
    field = field + ",trustLevel" if params[:trustLevel] == "true"
    field = field + ",level" if params[:level] == "true"
    field = field + ",awards" if params[:awards] == "true"
    field = field + ",homepage" if params[:homepage] == "true"
    #name,suffix,prefix,region,area,homepage,logo,memberCount,password"
    field = field + ",name" if params[:teamname] == "true"
    field = field + ",logo" if params[:logo] == "true"
    field = field + ",suffix" if params[:suffix] == "true"
    field = field + ",prefix" if params[:prefix] == "true"
    field = field + ",area" if params[:area] == "true"
    field = field + ",memberCount" if params[:count] == "true"
    #actuall fetching
    link.gsub! "<fields>", field
    link.gsub! "<ids>", id unless more
    if more
      temps = ""
      id.each do |i|
        temps = "#{temps},#{i.to_s}"
      end
      link.gsub! "<ids>", temps
    end
    uri = URI("#{link}")
    response = Net::HTTP.get(uri)
    r = JSON.parse(response)
    request = Request.create(typ: type, body: response)
    r = {
      request_id: request.id,
      body: r
    }
    render json: {status: 200, result: r}
  end

  # PATCH/PUT /requests/1
  # PATCH/PUT /requests/1.json
  def update
    respond_to do |format|
      if @request.update(request_params)
        format.html { redirect_to @request, notice: 'Request was successfully updated.' }
        format.json { render :show, status: :ok, location: @request }
      else
        format.html { render :edit }
        format.json { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /requests/1
  # DELETE /requests/1.json
  def destroy
    @request.destroy
    respond_to do |format|
      format.html { redirect_to requests_url, notice: 'Request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_request
      @request = Request.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def request_params
      params.require(:request).permit(:type, :body)
    end
end
