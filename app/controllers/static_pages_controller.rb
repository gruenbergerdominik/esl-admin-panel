class StaticPagesController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:cup, :rps]
  require 'net/http'
  require 'uri'
  require 'json'

  def index
    count = 10
    @cups_name = CupStat.where.not(finished: nil, start: nil).order("finished DESC").limit(10).pluck(:league)
    @cups_checked_in = CupStat.where.not(finished: nil, start: nil).order("finished DESC").limit(10).pluck(:checked_in)
    @cups_signedup_in = CupStat.where.not(finished: nil, start: nil).order("finished DESC").limit(10).pluck(:signed_up)
  end

  def data
    
    end

    def history
      @request = Request.all
    end

    def statistic
    
    end

    def eslm
      
    

  end

  def cups
    
  end

  def rps
    url = params[:url]
    uri = URI("#{url}")
    response = Net::HTTP.get(uri)
    id = response.split('[league-id]="')[1].split('"')[0]
    data = {
        id: id,
        results: {},
        rounds: 1,
        matches: []
    }
    rankings_url = "https://api.eslgaming.com/play/v1/leagues/#{id}/ranking?limit=8"
    info_url = "https://api.eslgaming.com/play/v1/leagues/#{id}"
    #ranking
    r_uri = URI("#{rankings_url}")
    body = Net::HTTP.get(r_uri)
    r = JSON.parse(body)
    data["results"] = r["ranking"]
    #infos
    i_uri = URI("#{info_url}")
    info = Net::HTTP.get(i_uri)
    info = JSON.parse(info)
    size = info["contestants"]["max"]
    round_number = 1
    round=2
    loop do
      if round >= size
        break
      end
      round_number += 1
      round = round * 2
    end
    round_number -= 1
    data["rounds"] = round_number
    #Matches
    #matches_url = "https://api.eslgaming.com/play/v1/leagues/#{id}/matches?round="
    #matches = []
    #m1_uri = URI("#{matches_url}#{round_number}")
    #match = Net::HTTP.get(m1_uri)
    #match = JSON.parse(match)
    #match.each do |m|
    #  u = url + "/match/" + m["id"].to_s + "/"
    #  ur = URI(u)
    #  body = Net::HTTP.get(ur)
    #  table = '<table round="'+ (size-1).to_s + '">' + body.split('<table class="lol_stats">')[1].split("</table>")[0] + "</table>"
    #  matches.push(table.force_encoding('UTF-8'))
    #end
    #m2_uri = URI("#{matches_url}#{round_number-2}")
    #match = Net::HTTP.get(m2_uri)
    #match = JSON.parse(match)
    #match.each do |m|
    #  u = url + "/match/" + m["id"].to_s + "/"
    #  ur = URI(u)
    #  body = Net::HTTP.get(ur)
    #  table = '<table round="'+ (size-1).to_s + '">' + body.split('<table class="lol_stats">')[1].split("</table>")[0] + "</table>"
    #  matches.push(table.force_encoding('UTF-8'))
    #end
    #data["matches"] = matches'
    render json: {result: data}
  end

  def cup
    link = 'https://api.eslgaming.com/play/v1/leagues?fields=id,name,uri,mode,skillLevel,timeline,contestants&path=<path>&types=cup,swiss,ffa&states=,finished&includeHidden=1'
    navnode = params[:navnode]
    navnode.gsub! "/", "%2F"
    puts navnode
    link.sub! "<path>", "#{navnode}"
    uri = URI("#{link}")
    response = Net::HTTP.get(uri)
    r = JSON.parse(response)
    r.each do |c|
      cup = c[1]
      if true
        stat = CupStat.where(league: "#{cup["id"]}").first_or_initialize
        #game & edition
        uri = cup["uri"]
        uri = uri.split("/play/")[1].split("/")
        #finished & start time
        timeline = cup["timeline"]
        finished = nil
        start = nil
        finished = DateTime.parse(timeline["finished"]["begin"]) unless timeline["finished"]["begin"].nil?
        start = DateTime.parse(timeline["inProgress"]["begin"]) unless timeline["inProgress"]["begin"].nil?
        puts finished
        puts "\n"
        puts start
        stat.assign_attributes(finished: finished, start: start,league: cup["id"].to_s, name: cup["name"]["full"], uri: cup["uri"], mode: cup["mode"], skill_level: cup["skillLevel"], signed_up: cup["contestants"]["signedUp"], checked_in: cup["contestants"]["checkedIn"], size: cup["contestants"]["max"], game: uri[0], edition: uri[1])
        stat.save! if stat.changed?
      end
    end
    render json: {status: 200, message: "All Cups created!"}
  end

end
