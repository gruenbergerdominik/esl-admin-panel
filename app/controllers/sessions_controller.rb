class SessionsController < ApplicationController
  layout 'pubg_eslm_layout'
  skip_before_action :verify_authenticity_token, only: [:login]
  def index
    
  end

  def login
    if params[:password] == "ESL_MEISTERSCHAFT_SPRING_PUBG"
      cookies[:_PUBG_ESLM_COOKIE_9374843] = { value: "3.14159265359", expires: 24.hour }
      redirect_to ranking_playdays_path
    else
      redirect_to sessions_path
    end
  end
end
