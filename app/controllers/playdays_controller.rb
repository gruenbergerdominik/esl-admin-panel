class PlaydaysController < ApplicationController
    layout 'pubg_eslm_layout'
    skip_before_action :verify_authenticity_token, only: [:create]
    before_action :checkSession, only: [:index, :new]
    
    def index
    
    
    end

    def show
        checkSession
        @Playday = Playday.find(params[:id])
    end


    def new
        
    end

    def ranking
        @ranking = {}
        Team.all.each do |t|
            @ranking[t.id] = {k: 0, p: 0}
        end
        scores = Score.all
        scores.each do |s|
            id = s.team_id
            k = s.kills
            p = s.points
            puts 490/100*50
            pro = Playday.find(s.playday_id).prozent
            p = p * (pro * 0.01)
            h_k = @ranking[id][:k]
            h_p = @ranking[id][:p]
            #puts "#{id} #{h_k} #{h_p}"
            @ranking[id] = {k: k + h_k, p: p + h_p}
        end
        @ranking = @ranking.sort do |a,b|
            b[1][:p] <=> a[1][:p]
        end
        puts @ranking
    end

    def create
        playday = Playday.create(number: params[:nummer], division: params[:division], prozent: params[:prozent])
        teams = Team.where(division: params[:division])
        teams.each do |t|
            playday.scores << t.scores.create(kills: 0, points: 0)
        end
        redirect_to playdays_path
    end
end