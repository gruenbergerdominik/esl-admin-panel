class RpTicketsController < ApplicationController
  before_action :set_rp_ticket, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token, only: [:receive]
  # GET /rp_tickets
  # GET /rp_tickets.json
  def index
    @rp_tickets = RpTicket.all
  end

  # GET /rp_tickets/1
  # GET /rp_tickets/1.json
  def show
  end

  # GET /rp_tickets/new
  def new
    @rp_ticket = RpTicket.new
  end

  # GET /rp_tickets/1/edit
  def edit
  end

  # POST /rp_tickets
  # POST /rp_tickets.json
  def create
    @rp_ticket = RpTicket.new(rp_ticket_params)

    respond_to do |format|
      if @rp_ticket.save
        format.html { redirect_to @rp_ticket, notice: 'Rp ticket was successfully created.' }
        format.json { render :show, status: :created, location: @rp_ticket }
      else
        format.html { render :new }
        format.json { render json: @rp_ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  def receive
    rp_ticket = RpTicket.create(url: params[:url], title: params[:title], date: params[:date], opendBy: params[:opendby] ,addedBy: params[:addedby],mailed: false,done: false)

  end

  # PATCH/PUT /rp_tickets/1
  # PATCH/PUT /rp_tickets/1.json
  def update
    respond_to do |format|
      if @rp_ticket.update(rp_ticket_params)
        format.html { redirect_to @rp_ticket, notice: 'Rp ticket was successfully updated.' }
        format.json { render :show, status: :ok, location: @rp_ticket }
      else
        format.html { render :edit }
        format.json { render json: @rp_ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rp_tickets/1
  # DELETE /rp_tickets/1.json
  def destroy
    @rp_ticket.destroy
    respond_to do |format|
      format.html { redirect_to rp_tickets_url, notice: 'Rp ticket was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rp_ticket
      @rp_ticket = RpTicket.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rp_ticket_params
      params.require(:rp_ticket).permit(:url, :title, :date, :opendBy, :addedBy, :mailed, :done, :whenMailed, :whenDone)
    end
end
