class ScoresController < ApplicationController
    layout 'pubg_eslm_layout'
    skip_before_action :verify_authenticity_token, only: [:create]
    before_action :checkSession

    def create
        scores = params[:data]

        scores.each do |l|
            kills = l[1]["kills"]
            points = l[1]["points"]
            id = l[1]["id"]
            puts id
            score = Score.where(id: id).first
            score.kills = kills
            score.points = points
            score.save!
        end
    end

end
