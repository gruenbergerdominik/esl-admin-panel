json.extract! cup_stat, :id, :league, :name, :uri, :size, :mode, :timeline, :signed_up, :checked_in, :created_at, :updated_at
json.url cup_stat_url(cup_stat, format: :json)
