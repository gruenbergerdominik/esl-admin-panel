json.extract! rp_ticket, :id, :url, :title, :date, :opendBy, :addedBy, :mailed, :done, :whenMailed, :whenDone, :created_at, :updated_at
json.url rp_ticket_url(rp_ticket, format: :json)
