class AddGameToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :cup_stats, :game, :string
    add_column :cup_stats, :edition, :string
  end
end
