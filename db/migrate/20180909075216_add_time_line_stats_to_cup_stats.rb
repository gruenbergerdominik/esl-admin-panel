class AddTimeLineStatsToCupStats < ActiveRecord::Migration[5.2]
  def change
    add_column :cup_stats, :finished, :datetime
    add_column :cup_stats, :start, :datetime
    remove_column :cup_stats, :timeline
  end
end
