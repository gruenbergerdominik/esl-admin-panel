class ChangeDoneTypeToBoolean < ActiveRecord::Migration[5.2]
  def change
    change_column :rp_tickets, :done, :boolean
  end
end
