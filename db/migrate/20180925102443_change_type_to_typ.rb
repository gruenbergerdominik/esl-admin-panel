class ChangeTypeToTyp < ActiveRecord::Migration[5.2]
  def change
    rename_column :requests, :type, :typ
  end
end
