class CreateRpTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :rp_tickets do |t|
      t.string :url
      t.string :title
      t.date :date
      t.string :opendBy
      t.string :addedBy
      t.boolean :mailed
      t.string :done
      t.date :whenMailed
      t.date :whenDone

      t.timestamps
    end
  end
end
