class CreateRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :requests do |t|
      t.string :type
      t.text :body

      t.timestamps
    end
  end
end
