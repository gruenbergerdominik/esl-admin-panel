class CreatePlaydays < ActiveRecord::Migration[5.2]
  def change
    create_table :playdays do |t|
      t.string :division
      t.integer :number

      t.timestamps
    end
  end
end
