class ChangeProzentToFloat < ActiveRecord::Migration[5.2]
  def change
    change_column :playdays, :prozent, :float 
  end
end
