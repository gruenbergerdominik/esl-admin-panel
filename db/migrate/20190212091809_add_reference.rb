class AddReference < ActiveRecord::Migration[5.2]
  def change
    add_reference :scores, :team, index: true 
  end
end
