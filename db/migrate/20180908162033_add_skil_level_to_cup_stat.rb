class AddSkilLevelToCupStat < ActiveRecord::Migration[5.2]
  def change
    add_column :cup_stats, :skill_level, :string
  end
end
