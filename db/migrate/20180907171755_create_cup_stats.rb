class CreateCupStats < ActiveRecord::Migration[5.2]
  def change
    create_table :cup_stats do |t|
      t.integer :league
      t.string :name
      t.string :uri
      t.string :size
      t.string :mode
      t.text :timeline
      t.integer :signed_up
      t.integer :checked_in

      t.timestamps
    end
  end
end
