class AddReferenceToPlayday < ActiveRecord::Migration[5.2]
  def change
    add_reference :scores, :playday, index: true
  end
end
