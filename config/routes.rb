Rails.application.routes.draw do
  resources :teams
  resources :scores
  resources :playdays do
    collection do
      get 'ranking'
    end
  end
  resources :sessions, only: [:index] do
    collection do
      post "login"
    end
  end
  #resources :requests
  #resources :cup_stats
  #resources :rp_tickets do
    #collection do
      #post 'receive'
    #end
  #end
  #resources :static_pages, only: [:index], path: "" do
    #collection do
      #get 'statistics'
      #get 'data'
      #get 'history'
      #get 'cups'
      #get 'rp'
      #post 'rps'
      #get 'eslm'
      #post 'cup'
    #end
  #end
  root 'teams#index'
end
