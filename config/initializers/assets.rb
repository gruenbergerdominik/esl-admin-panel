# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')
Rails.application.config.assets.precompile += %w( plugins/sortable/datatables.css )
Rails.application.config.assets.precompile += %w( plugins/sortable/datatables.js )
Rails.application.config.assets.precompile += %w( home.css )
Rails.application.config.assets.precompile += %w( home.css )
Rails.application.config.assets.precompile += %w(images/* stylesheets/* javascripts/*)
Rails.application.config.assets.precompile += %w( plugins/sidebar-nav/dist/sidebar-nav.js )
Rails.application.config.assets.precompile += %w( plugins/jquery.slimscroll.js )
Rails.application.config.assets.precompile += %w( plugins/waves.js )
Rails.application.config.assets.precompile += %w( plugins/waypoints/lib/jquery.waypoints.js )
Rails.application.config.assets.precompile += %w(plugins/counterup/jquery.counterup.min.js)
Rails.application.config.assets.precompile += %w(plugins/chartist-js/dist/chartist.js)
Rails.application.config.assets.precompile += %w(plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.js)
Rails.application.config.assets.precompile += %w(plugins/jquery-sparkline/jquery.charts-sparkline.js)
Rails.application.config.assets.precompile += %w(plugins/custom.js)
Rails.application.config.assets.precompile += %w(plugins/dashboard1.js)
Rails.application.config.assets.precompile += %w(plugins/toast-master/js/jquery.toast.js)
Rails.application.config.assets.precompile += %w(plugins/sidebar-nav/dist/sidebar-nav.css)
Rails.application.config.assets.precompile += %w(plugins/toast-master/css/jquery.toast.css)
Rails.application.config.assets.precompile += %w(plugins/morrisjs/morris.css)
Rails.application.config.assets.precompile += %w(plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css)
Rails.application.config.assets.precompile += %w(plugins/chartist-js/dist/chartist.css)
Rails.application.config.assets.precompile += %w(animate.css)
Rails.application.config.assets.precompile += %w(style.css)
Rails.application.config.assets.precompile += %w( colors/default.css)
Rails.application.config.assets.precompile += %w(font-aewsome.5.css)
Rails.application.config.assets.precompile += %w( pubg.css )
# Precompile additional assets.
# application.js, application.scss, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )
